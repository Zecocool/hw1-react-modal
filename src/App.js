import React, { Component } from "react";
import Modal from "./Components/modal/Modal";
import ProductList from "./Components/productsList/ProductList";
import favoriteIcon from "./image/ferstMoney.png";
import basketIcon from "./image/secondBasket.png"

import styles from "./Style.module.scss";



class App extends Component {
  constructor() {
    super();
    this.state = {
      products: [],
      productsInFavorite: [],
      productsInBasket: [],
      modal: {
        visible: false,
        modalId: null,
        submitFunction: null,
      },
    };
  }

  componentDidMount() {
    fetch("productCollection.json")
      .then((response) => response.json())
      .then((products) => this.setState({ products: products }));

    const productsInFavorite = localStorage.getItem("productsInFavorite");
    if (productsInFavorite) {
      this.setState({
        productsInFavorite: JSON.parse(productsInFavorite),
      });
    } else {
      localStorage.setItem("productsInFavorite", JSON.stringify([]));
    }

    const productsInBasket = localStorage.getItem("productsInBasket");
    if (productsInBasket) {
      this.setState({
        productsInBasket: JSON.parse(productsInBasket),
      });
    } else {
      localStorage.setItem("productsInBasket", JSON.stringify([]));
    }
  }

  updateFavorite = () => {
    const productsInFavorite = localStorage.getItem("productsInFavorite");
    if (productsInFavorite) {
      this.setState({
        productsInFavorite: JSON.parse(productsInFavorite),
      });
    }
  };

  updateBasket = () => {
    const productsInBasket = localStorage.getItem("productsInBasket");
    if (productsInBasket) {
      this.setState({
        productsInBasket: JSON.parse(productsInBasket),
      });
    }
  };

  openModal = (modalId, submitFunction) => {
    this.setState({
      modal: {
        visible: true,
        modalId,
        submitFunction,
      },
    });
  };

  closeModal = () => {
    this.setState({
      modal: {
        visible: false,
        modalId: null,
        submitFunction: null,
      },
    });
  };

  render() {
    const { products, productsInFavorite, productsInBasket, modal } = this.state;

    return (
      <div className={styles.App}>
        <div className={styles.productCounts}>
          <div
            className={styles.favoriteIcon}
            onClick={() => console.log(productsInFavorite)}
          >
            <img src={favoriteIcon} alt="" />
            <span className={styles.productCount}>
              {productsInFavorite.length}
            </span>
          </div>
          <div
            className={styles.favoriteIcon}
            onClick={() => console.log(productsInBasket)}
          >
            <img src={basketIcon} alt="" />
            <span className={styles.productCount}>
              {productsInBasket.length}
            </span>
          </div>
        </div>
        <ProductList
          products={products}
          updateFavorite={this.updateFavorite}
          updateBasket={this.updateBasket}
          openModal={this.openModal}
        />
        {modal.visible && (
          <Modal
            visible={modal.visible}
            modalId={modal.modalId}
            submitFunction={modal.submitFunction}
            closeModal={this.closeModal}
          />
        )}
      </div>
    );
  }
}

export default App;
