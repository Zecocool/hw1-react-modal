const modalSettings = [
    {
        modalId : "addToBasket",
        settings: {
            header: "Adding a product to the cart",
            closeButton: true,
            text: "You are trying to add a product to the cart. Continue? ",
            actions:[
                {
                    type:"submit",
                    background: "red",
                    text: "Let's go!"
                },
                {
                    type:"cancel",
                    background:"green",
                    text: "I do not need it"
                }
            ]
        }
    }
];

export default modalSettings;