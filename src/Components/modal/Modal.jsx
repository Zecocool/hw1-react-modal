import React, { Component } from 'react';
import Button from '../button/Button';
import styles from "./Module.module.scss";
import PropTypes  from 'prop-types';
import modalSettings from "./modalSettings";

export default class Modal extends Component {
    constructor(props) {
      super(props);
      this.state = {
        settings: {},
      };
    }
  


    componentDidMount() {
        const modal = modalSettings.find(
          (item) => item.modalId === this.props.modalId
        );
        this.setState({ settings: modal.settings });
      }
    
  
    render() {
      const { header, closeButton, text, actions } = this.state.settings;
      return (
        this.props.visible && (
          <div className={styles.Modal} onClick={this.props.closeModal}>
            <div
              className={styles.ModalContent}
              onClick={(e) => e.stopPropagation()}
            >
              <div className={styles.ModalHeader}>
                {header}
                {closeButton && (
                  <Button
                    text="X"
                    background="red"
                    onClick={this.props.closeModal}
                  />
                )}
              </div>
              <div className={styles.ModalBody}>{text}</div>
              <div className={styles.ModalFooter}>
                {actions &&
                  actions.map((item, index) => (
                    <Button
                      text={item.text}
                      onClick={
                        item.type === "submit" 
                          ? () => {
                              this.props.submitFunction();
                              this.props.closeModal();
                            } 
                          : this.props.closeModal
                      }
                      key={Date.now() + index}
                    />
                  ))}
              </div>
            </div>
          </div>
        )
      );
    }
  }
  
  Modal.propTypes = {
    visible: PropTypes.bool,
    modalId: PropTypes.string,
    submit: PropTypes.func,
    closeModal: PropTypes.func,
  };
  