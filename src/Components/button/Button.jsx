import React, { Component } from "react";
import styleBtn from "./Button.module.scss";
import PropTypes from "prop-types";

export default class Button extends Component {
  render() {
    return (
      <button
        className={styleBtn.Button}
        style={{ background: this.props.background }}
        onClick={this.props.onClick}
      >
        {this.props.text}
      </button>
    );
  }
}

Button.propTypes = {
  background: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
}

Button.defaultProps = {
  background: "red"
};

