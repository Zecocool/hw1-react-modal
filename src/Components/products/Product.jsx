import React, { Component } from 'react';
import PropTypes from "prop-types";
import Button from '../button/Button';
import styles from "./Product.module.scss"
import favoriteMoney from "../../image/ferstMoney.png"
import favoriteMoneyCommplete from "../../image/secondMoney.png"

export default class Product extends Component {
    constructor(props) {
        super(props)
        this.state ={
            inBasket: false,
            inFavorite: false,
        }
    }

        componentDidMount() {
            const { product } = this.props;
            const productsInFavoriteArray = JSON.parse(localStorage.getItem("productsInFavorite")) || [];
            const productInFavorite = productsInFavoriteArray.find(p => p.article === product.article);
            if (productInFavorite) this.setState({ inFavorite: true });
        
            const productsInBasketArray = JSON.parse(localStorage.getItem("productsInBasket")) || [];
            const productInBasket = productsInBasketArray.find(p => p.article === product.article);
            if (productInBasket) this.setState({ inBasket: true });
          }
        
          addToFavorite = () => {
            const { product } = this.props;
            const productsInFavoriteArray = JSON.parse(localStorage.getItem("productsInFavorite")) || [];
            if (productsInFavoriteArray.find(p => p.article === product.article)) return;
            const updatedProductsInFavoriteArray = [...productsInFavoriteArray, product];
            localStorage.setItem("productsInFavorite", JSON.stringify(updatedProductsInFavoriteArray));
            this.setState({ inFavorite: true });
            this.props.updateFavorite();
          };
        
          removeFromFavorite = () => {
            const { product } = this.props;
            const productsInFavoriteArray = JSON.parse(localStorage.getItem("productsInFavorite")) || [];
            const updatedProductsInFavoriteArray = productsInFavoriteArray.filter(p => p.article !== product.article);
            localStorage.setItem("productsInFavorite", JSON.stringify(updatedProductsInFavoriteArray));
            this.setState({ inFavorite: false });
            this.props.updateFavorite();
          };
        
          toggleFavorite = () => {
            if (this.state.inFavorite) this.removeFromFavorite();
            else this.addToFavorite();
          };
        
          addToBasket = () => {
            const { product } = this.props;
            const productsInBasketArray = JSON.parse(localStorage.getItem("productsInBasket")) || [];
            if (productsInBasketArray.find(p => p.article === product.article)) return;
            const updatedProductsInBasketArray = [...productsInBasketArray, product];
            localStorage.setItem("productsInBasket", JSON.stringify(updatedProductsInBasketArray));
            this.setState({ inBasket: true });
            this.props.updateBasket();
          };
        
    
    render() {
    const { product } = this.props;
    const { title, price, imgUrl, color } = product;
        return (
            <div className ={styles.Product}>
                <div className={styles.Favorite} onClick= {this.toggleFavorite}>
                    <img src={this.state.inFavorite ? favoriteMoneyCommplete : favoriteMoney } alt="" />
                </div>
                <div className={styles.imageWrapper}>
                    <img src={imgUrl} alt="" />
                </div>
                <div>
                    <h1>
                        {title}
                    </h1>
                    <h2>
                    Color: {color}
                    </h2>
                    <h3>
                      Price: {price}
                    </h3>
                </div>
                <Button text ={this.state.inBasket ? "Get into the cat" : "Up to the cat" } 
                background ={ this.state.inBasket ? "white" : "magenta"}
                onClick = {() => {
                    !this.state.inBasket && this.props.openModal("addToBasket", this.addToBasket)
                }}
                ></Button>
            </div>
        );
    }
}

Product.propTypes = {
    product: PropTypes.object,
    updateFavorite: PropTypes.func,
    updateBasket: PropTypes.func,
    openModal: PropTypes.func,
  };
